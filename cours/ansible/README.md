# Ansible

**Ansible est un outil de gestion et déploiement de configuration, il est libre et opensource.**

Par "gestion et déploiement de configuration", on entend qu'il est capable de :

- stocker toutes les configurations de toutes les machines d'un parc
- déployer les configurations sur les machines, sans connexion manuelle de l'admin

Une fois Ansible en place, les admins ne se connectent plus directement aux machines pour les configurer ou y installer des applications.

A la place, on ordonne à Ansible de se connecter aux machines et d'y déposer les configurations que l'on souhaite.

![You fool](./pics/you_fool.jpg)

# Sommaire

- [Ansible](#ansible)
- [Sommaire](#sommaire)
- [I. Le modèle Ansible](#i-le-modèle-ansible)
  - [1. Les machines](#1-les-machines)
  - [2. SSH](#2-ssh)
  - [3. Python](#3-python)
  - [4. Les fichiers Ansible](#4-les-fichiers-ansible)
  - [5. Récap](#5-récap)
- [II. Le langage Ansible](#ii-le-langage-ansible)
  - [1. Format YML](#1-format-yml)
    - [A. Simple variable](#a-simple-variable)
    - [B. Liste](#b-liste)
    - [C. Dictionnaire](#c-dictionnaire)
  - [2. Task](#2-task)
  - [3. Inventaire](#3-inventaire)
  - [4. Playbook](#4-playbook)
  - [5. Les commandes Ansible](#5-les-commandes-ansible)
    - [A. La commande `ansible`](#a-la-commande-ansible)
    - [B. La commande `ansible-playbook`](#b-la-commande-ansible-playbook)
- [III. Range ta chambre](#iii-range-ta-chambre)
  - [1. Les rôles](#1-les-rôles)
  - [2. Les variables d'inventaires](#2-les-variables-dinventaires)
  - [3. Idempotence](#3-idempotence)
- [IV. Utilisation d'Ansible](#iv-utilisation-dansible)
  - [1. Inconvénients](#1-inconvénients)
    - [Skill](#skill)
    - [Apparente perte de temps](#apparente-perte-de-temps)
  - [2. Bénéfices](#2-bénéfices)
    - [Gain de temps](#gain-de-temps)
    - [Sécurité](#sécurité)
    - [Infrastructure as Code](#infrastructure-as-code)
    - [Maintenabilité](#maintenabilité)
    - [Documentation d'infrastructure](#documentation-dinfrastructure)

# I. Le modèle Ansible

## 1. Les machines

Ansible définit deux types de machines :

- **le(s) *control node(s)***
  - ces machines possède tout le code Ansible
  - on exécute, depuis ces machines, des commandes `ansible` et `ansible-playbook`
  - ces machines envoie de la configuration vers les *machines de destination*
- **la(les) machine(s) de destination**
  - ces machines reçoivent la conf envoyée par Ansible, depuis les *control nodes*

Ainsi :

- les *control nodes* sont les machines des admins
- les *machines de destination* sont les serveurs du parc

## 2. SSH

**SSH a une place centrale dans le modèle Ansible : c'est grâce à SSH que Ansible se connecte depuis les *control nodes* vers les *machines de destination*.**

Dans une installation classique d'Ansible :

- les *control nodes* ont une paire de clé SSH
  - vu que les *control nodes* sont les machines des admins, on parle ici des clés SSH de l'admin
- les *machines de destination* possède les clés publiques
  - ainsi, les admins, depuis leurs *control nodes* peuvent se connecter sans mot de passe à toutes les *machines de destination*

Ainsi, pour qu'Ansible fonctionne correctement, il est nécessaire d'établir, au préalable, un échange de clé entre tous les *control nodes* et toutes les *machines de destination*.

Question de bonne pratique, il existe sur les *machines de destination* un utilisateur système PAR administrateur.  
Chacun de ces utilisateurs système est autorisé à accéder à l'identité de root grâce à la commande `sudo` (nécessaire pour les opérations d'administration).  
Cela permet de garder une traçabilité des actions effectuées sur les *machines de destination*.

> C'était déjà -normalement- le cas avant d'utiliser Ansible : les admins utilisaient leurs clés pour se connecter au *machines de destination*. C'est d'ailleurs une des forces d'Ansible : les admins utilisent toujours les outils habituels pour déployer de la conf (SSH + échange de clé + un compte pour chaque admin sur les *machines de destination*) ; simplement, les commandes ne sont plus exécutées manuellement, elles sont réalisées par Ansible.

## 3. Python

**Python aussi occupe une place primordiale dans le fonctionnement d'Ansible.**

En effet, pour que les *machines de destination* puisse appliquer la configuration que les *control nodes* demandent, Python sera utilisé sur les *machines de destination*.

## 4. Les fichiers Ansible

**Les fichiers Ansible sont au format `.yml`.**

**Ces fichiers ont pour but de décrire la configuration qui sera appliquée sur les *machines de destination*.**

**On dit que Ansible utilise un langage déclaratif** : on définit un état dans lequel la machine doit être, mais on ne précise jamais les commandes pour atteindre cet état.  
C'est Ansible qui détermine les commandes adaptées à exécuter pour atteindre cet état.

---

Par exemple, on va dire dans un fichier au format `.yml` qu'un paquet précis doit être "présent" sans préciser que la commande à utiliser sera `yum install` sur un système RedHat, ou bien `apt-get install` sur un système Debian.  
Ansible déduira tout seul quelle commande doit être utilisée, en fonction de l'OS de la *machine de destination*.

## 5. Récap

Le modèle Ansible :

- *control nodes*
  - machines des admins
  - possèdent une paire de clés
  - possèdent tous les fichiers `.yml` d'Ansible
  - peut se connecter en SSH à toutes les machines du parc
  - a `ansible` et `python` installé
- *machines de destination*
  - serveurs du parc
  - les admins s'y connectent *via* SSH (par échange de clé)
  - a uniquement `python` installé

# II. Le langage Ansible

Les fichiers Ansible sont au format YML ou YAML, l'extension utilisée est `.yml` ou `.yaml`.

> Pas de différences entre YML et YAML, choisissez celui que vous préférez.

## 1. Format YML

**Le YML est un langage qui permet de stocker des données.**

L'indentation est déterminante en YML (comme dans le langage Python si vous êtes familiers avec).  
En effet, en YML, il n'existe pas de blocs délimités par des accolades `{ }`, ni même de fins de ligne indiquées par `;`.

> On ne trouve donc pas d'algorithmique dans des fichiers YML, mais uniquement des données. YAML est analogue à JSON, ou HTML ou encore XML.

### A. Simple variable

Déclaration d'une variable en YML :

```yml
super_variable: test
```

La variable `super_variable` contient la valeur `test`.

### B. Liste

Il est possible de déclarer des listes en YML :

```yml
users:
  - it4
  - rick
  - morty
  - apache
```

Cette liste `users` contient 4 éléments : `it4`, `toto`, `rick` et `morty`.

### C. Dictionnaire

Un dictionnaire permet de stocker plusieurs variables sous une seule entité :

```yml
users:
  - it4:
      shell: /bin/bash
      homedir: /home/it4
  - rick:
      shell: /bin/bash
      homedir: /home/rick
  - morty:
      shell: /bin/bash
      homedir: /home/morty
  - apache:
      shell: /usr/sbin/nologin
      homedir: /var/www/html
```

Ici, `users` est une liste. Cette liste contient 4 dictionnaires : `it4`, `rick`, `morty` et `apache`.

---

Maintenant qu'on a vu comment écrire du YML, voyons comment cela s'applique à Ansible.

## 2. Task

**Une *task* c'est l'entité atomique que Ansible manipule.**

**Une *task* fait forcément appel à un *module*** afin qu'Ansible sache quoi faire.

Ainsi, par exemple, si on souhaite installer le paquet Apache sur une machine, on va déclarer au format YML :

- un nom, arbitraire pour cette *task*
- que l'on veut utiliser le *module* `package`
  - le *module* `package` permet d'installer des paquets
  - ce *module* sait quelle commande exécuter en fonction de l'OS afin d'installer un paquet
- le nom du paquet à installer
- que l'on veut que le paquet soit `present` sur la machine

---

Concrètement, cette *task* pourrait ressembler à :

```yml
- name: Install Apache package
  package:
    name: httpd
    state: present
```

Dans cet exemple :

- `Install Apache package` est le nom arbitraire de la tâche
- `package` est le nom du *module* utilisé
- `name: httpd`
  - est une clause obligatoire quand on utilise le *module* `package`
  - c'est le nom du paquet sur lequel agir, ici `httpd`
- `state: present`
  - est une clause obligatoire quand on utilise le *module* `package`
  - désigne l'état du paquet que l'on souhaite, ici `present`

Ce fichier, une fois déployé sur une machine Debian par exemple, résultera en l'exécution d'un `sudo apt-get install httpd` sur la machine Debian.

> Chaque module possède une page dédiée dans la documentation Ansible. [Ici la documentation du *module* `package` par exemple.](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html)

## 3. Inventaire

**Un *inventaire* Ansible (ou *inventory*) désigne un fichier Ansible, au format `.ini` ou `.yml` qui liste des machines.**

Les machines listées sont regroupées dans des groupes, dont le nom peut être arbitraire.

---

Exemple de fichier d'inventaire, au format `.ini` :

```ini
[ynov]
node1.ynov.prod
node2.ynov.prod

[lab]
node1.lab
node2.lab
```

Dans ce fichier, on a notamment un groupe d'hôte appelé `ynov` qui contient deux machines : `node1.ynov.prod` et `node2.ynov.prod`.

> Cela sous-entend que les *control nodes* sont capables d'effectuer une connexion SSH vers `node1.ynov.prod` et `node2.ynov.prod`.

## 4. Playbook

Le *playbook* est un fichier Ansible, format `.yml` toujours.

Un *playbook* fait le lien entre les *tasks* et l'*inventaire*.

Le *playbook* détermine quelles *tasks* seront exécutées sur quelle machine de l'*inventaire*.

---

Un exemple de *playbook* qui s'appellerait `apache.yml` :

```yml
- name: Setup Apache servers on YNOV group
  hosts: ynov # ce groupe est défini dans le fichier d'inventaire

  tasks:
  - name: Install Apache
    package:
      name: httpd
      state: latest
```

## 5. Les commandes Ansible

**Une fois que l'on a écrit au moins un *playbook*, au format `.yml`, ainsi qu'un *inventaire*, on peut alors exécuter des commandes Ansible.**

Les commandes utilisées sont :

- **`ansible`**
  - permet d'effectuer des opérations one-shot sur une machine ou un groupe
- **`ansible-playbook`**
  - permet d'envoyer la conf définie dans un *playbook* sur une machine

Ces commandes sont exécutées depuis un *control node*. Le but de ces commandes est d'envoyer de la configuration sur les **machines de destination**.

---

### A. La commande `ansible`

Cette commande permet d'effectuer des opérations one-shot depuis la ligne de commandes.

On peut se servir de l'*inventaire* afin d'exécuter des tâches one-shot sur des machines ou groupes qui y sont déclarés.

Exemples :

```bash
# tester que le control node peut correctement joindre les *machines de destination* du groupe "ynov"
$ ansible ynov -i hosts.ini -m ping

# installer un paquet sur les machines du groupe "ynov"
$ ansible ynov -i hosts.ini -m package -a "name=apache state=present"
```

Utiliser la commande `ansible`, on parle d'utiliser `ansible` de façon *ad-hoc*. [Une page de la documentation y est dédiée.](https://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)

### B. La commande `ansible-playbook`

La commande `ansible-playbook` permet de lire le contenu d'un *playbook* et d'appliquer son contenu. 

Pour rappel, un *playbook* désigne un fichier dans lequel on déclare quelle *task* doit être appliquée sur quelle *machine de destination*.

Exemples :

```bash
# déploiement du playbook présenté plus haut : apache.yml
$ ansible-playbook -i hosts.ini apache.yml
```

# III. Range ta chambre

On pourrait gérer un parc entier en écrivant qu'un unique fichier, qui contiendrait toutes les configurations.

Ce serait très difficilement maintenable. Pour améliorer la maintenabilité, et nous permettre d'accéder à d'autres bénéfices que l'on verra par la suite, on éclate la configuration Ansible dans plusieurs fichiers YML.

On va voir dans cette section comment ranger le dépôt Ansible, en utilisant des patterns recommandés.

## 1. Les rôles

Un *role* est un ensemble de *task*. Voilà, tout est dit en vrai !

Plus spécifiquement, on va regrouper sous un *role* toutes les *tasks* qui correspondent à la mise en place d'une application spécifique.

Par exemple, on pourrait trouver un *role* appelé `apache` qui contiendrait toutes les *tasks* nécessaires au bon fonctionnement d'Apache.  
Chacun des poitns suivants serait une ou plusieurs *tasks* :

- installation d'un paquet
- modification des fichiers de conf
- ouverture d'un port firewall
- démarrage du service `apache`

Concrètement, un *role* c'est juste un sous-dossier dans un dossier `roles/`. On aurait donc, pour notre exemple un dossier `roles/apache/` qui contiendrait toutes les *tasks* nécessaires au bon fonctionnement d'Apache, énumérées juste au dessus.

> La structure d'un *role* est spécifique elle aussi : les sous-dossiers du *role* sont aussi nommés de façon standard. Le [TP2 met en place ce pattern de *role*](../tp/2/README.md) et détaille l'architecture de fichiers à mettre en place.

## 2. Les variables d'inventaires

Il est possible de spécifier dans l'*inventaire* des variables spécifiques à une machine donneé ou un groupe de machines.

Ceci est réalisé via l'utilisation d'un dossier `inventories` et de sous-répertoires spécifiques.

Ceci aussi est exploré et exploité dans [le TP2](../tp/2/README.md).

## 3. Idempotence

**L'idempotence est un concept primordial dans Ansible.**

Ansible, avant de déployer une configuration, va tester sur la *machine de destination* que cette configuration n'a pas déjà été déployée.

> Par exemple, tester si un service est démarré avant d'exécuter la commande pour le démarrer.

Cela permet à Ansible de NE PAS effectuer l'action s'il détecte qu'elle a déjà été effectuée auparavant.

Lorsqu'Ansible effectue une action, on a dans les logs un `changed`. S'il a détecté que l'action avait déjà été réalisée, on obtient un `ok`.

**De façon concrète, l'idempotence c'est le fait que si on déroule un playbook une deuxième fois, il ne sort que des `ok`.**

C'est une bonne pratique Ansible que de construire des *playbooks* idempotents. Cela permet de gagner en performances, mais aussi de pouvoir agir comme un outil qui va contrôler la conformité avant d'agir.

![This is the way](./pics/idempotence.jpg)

# IV. Utilisation d'Ansible

Dans cette partie, on va s'attarder sur les bénéfices et les inconvénients liés à l'utilisation d'Ansible pour gérer un parc de machines.

## 1. Inconvénients

### Skill

**Manipuler Ansible demande du skill.**

Ansible possède sa propre logique, des fichiers qui ont une syntaxe spécifique, et des mécanismes particuliers qui permettent de mettre en place des infrastructures complexes.

Autant de nouvelles choses et nouveaux concepts à appréhender pour quelqu'un qui souhaite gérer des machines avec Ansible.

### Apparente perte de temps

Pour un admin confirmé, il est très simple et rapide de mettre en place une application donnée, en se connectant directement sur la *machine de destination*.

Ecrire des fichiers Ansible qui déploient cette conf à notre place peut apparaître comme un perte de temps, car il "suffirait" de se connecter à une machine pour y déployer de la conf.

## 2. Bénéfices

### Gain de temps

L'apparente perte de temps cité dans les inconvénients n'est en effet qu'une apparence.

Une fois qu'un *playbook* a été écrit une fois, il est réutilisable, et il est aisé de le redéployer.

Ainsi, on peut dire qu'on perd du temps au tout premier déploiement, quand on doit rédiger le *playbook* Ansible.  
Mais ce temps est très largemement récupéré car :

- aucune ou très peu de doc est nécessaire : le fichier Ansible parle de lui-même
- il est possible de redéployer autant de fois que l'on veut un *playbook*
- aucun temps ne sera perdu en lecture/compréhension de documentation : il suffit d'appliquer le *playbook*

### Sécurité

**L'utilisation d'Ansible amène à une automatisation complète du déploiement de configuration.**

Ainsi, les occurrences d'erreurs humaines ou de bugs liés à un déploiement sont bien moins nombreuses : un *playbook* correctement construit pourra s'exécuter de la même façon si on souhaite le réutiliser.

L'automatisation amène la standardisation : les déploiements deviennent standards, répétables, et donc robustes.

**La robustesse nous amène à une fiabilité des déploiements, et donc à une meilleure sécurité sur notre gestion de parc.**

### Infrastructure as Code

Une fois qu'Ansible est utilisé pour gérer le parc, on peut dire que toute l'infra est décrite dans des fichiers texte.

En effet, Ansible n'est composé que de fichiers texte, au format YML.

On parle alors d'Infrastructure as Code : notre infra tient dans des fichiers texte.

**On peut alors gérer nos fichiers Ansible comme on gérerait du code : en les stockant dans des dépôts git, en effectuant des tests dessus, en vérifiant de façon automatisée que les *playbooks* sont fonctionnels, etc.**

### Maintenabilité

Sûrement un des pus gros bénéfices apporté par Ansible : la maintenabilité de la configuration du parc.

Toute notre configuration tient dans des fichiers texte, qui sont facilement modifiables, adapatables.

Il devient aisé de mettre à jour l'ensemble du parc, ou de pousser une nouvelle configuration de sécurité quand elle est nécessaire.

Aussi, inutile désormais de se palucher des dizaines de pages de documentation pour comprendre comment mettre à jour une app : il suffit de se rendre dans les ficihers Ansible et effectuer un nouveau déploiement.

### Documentation d'infrastructure

**Le dépôt Ansible EST une documentation.**

Le dépôt Ansible contient, de façon exhaustive :

- la liste des machines du parc (*inventory*)
- la liste des configurations/applications que l'on déploie dans le parc (*roles*)
- l'état de chaque machine du parc : quelle conf a été déployée où

Ainsi, en ne se basant uniquement sur le contenu des fichiers Ansible, on peut comprendre comment est géré un parc, et ce, de façon exhaustive.

![Dark Side](./pics/dark_side.jpg)