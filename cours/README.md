# Cours

Le but de ce cours est d'explorer les techniques modernes de gestion d'infrastructure.

- [Intro Cloud](./cloud/README.md)
- [Ansible](./ansible/README.md)