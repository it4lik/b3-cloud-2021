# Intro Cloud

Ce cours a pour but de démystifier le terme "cloud" lorsqu'il est employé dans le milieu de l'IT.

---

"*A long time ago in a galaxy far far away, lived a bunch of human beings known as 'SysAdmins'. Those creatures used to do everything by hand: every piece of infrastructure was deployed manually (Servers, ELB, Network topology, Databases, etc.).*

*Then the cloud happened. It caused chaos, because of which many SysAdmins took a vacation forever. The remaining ones had to change their attitude and that was the birth of 'DevOps'. A new concept has been introduced: 'Infrastructure as Code' (IaC). The idea is to treat your infrastructure the same way you deal with your code. All changes can now be captured in code and versioned to code repository rather than a SysAdmin’s head.*"

> From https://blog.intercloud.com/en/a-sysadmins-worst-nightmare

![Because it's in the cloud](pics/its_in_the_cloud.jpg)

## Sommaire


- [Intro Cloud](#intro-cloud)
  - [Sommaire](#sommaire)
- [I. C koa 1 infra wess](#i-c-koa-1-infra-wess)
  - [1. Les ressources](#1-les-ressources)
  - [2. Gestion des ressources](#2-gestion-des-ressources)
  - [3. Exploitation des ressources](#3-exploitation-des-ressources)
  - [4. Applications](#4-applications)
- [II. Une Infra Cloud](#ii-une-infra-cloud)
  - [1. Les ressources](#1-les-ressources-1)
  - [2. Gestion des ressources](#2-gestion-des-ressources-1)
    - [A. Cloud Computing](#a-cloud-computing)
    - [B. Virtualisation](#b-virtualisation)
    - [C. Automatiser la création de VMs](#c-automatiser-la-création-de-vms)
  - [3. Exploitation des ressources](#3-exploitation-des-ressources-1)
  - [4. Applications](#4-applications-1)
    - [A. La conteneurisation](#a-la-conteneurisation)
    - [B. La CI/CD](#b-la-cicd)
- [III. Bénéfices](#iii-bénéfices)
  - [1. Securité](#1-securité)
  - [2. Optimisation et performances](#2-optimisation-et-performances)
  - [3. Réduction des coûts](#3-réduction-des-coûts)
- [IV. Vocabulaire lié](#iv-vocabulaire-lié)
  - [1. Infrastructure as Code](#1-infrastructure-as-code)
  - [2. Devops](#2-devops)
  - [3. Fournisseur Cloud](#3-fournisseur-cloud)
  - [4. as a Service](#4-as-a-service)
    - [A. SaaS](#a-saas)
    - [B. PaaS](#b-paas)
    - [C. IaaS](#c-iaas)
  - [5. Cloud-native](#5-cloud-native)
- [V. Acteurs du cloud](#v-acteurs-du-cloud)
  - [1. Fournisseurs Cloud](#1-fournisseurs-cloud)
  - [2. Open Source et Libre](#2-open-source-et-libre)
  - [3. CNCF](#3-cncf)
- [VI. P'tit bilan fast](#vi-ptit-bilan-fast)

# I. C koa 1 infra wess

Dans cette partie, on va grossièrement découper le terme "infrastructure" en plusieurs sections. L'idée est d'avoir une liste concise de ce qu'on entend lorsqu'on parle de gestion de parc informatique, de gestion d'infrastructure :

- des ressources, du **matériel**
- gestion des ressources, avec la **virtualisation**
- exploitation des ressources, en installant des **OS**
- exécution des **applications**, au sein des OS

Le but final de tout système informatisé étant de fournir un *service* porté par l'application qui est exécutée en bout de chaîne.

![Schéma infra](./pics/infra.png)

## 1. Les ressources

On peut grossièrement découper le matériel d'une infrastructure informatique typique en trois principales ressources :

- des serveurs
- des équipements réseau
  - routeur
  - switches
  - firewall
  - etc.
- des équipements stockage
  - baies de stockage
  - serveurs de stockage
  - etc.

➜ **Les serveurs** sont responsables du *compute* : ce sont eux qui effectuent des calculs, c'est à dire que ce sont eux qui exécutent les applications.

➜ Pour ce faire, **les serveurs reposent sur le réseau et le stockage.**

Le réseau afin de communiquer entre serveurs, et donc, entre applications.  
Le stockage afin de stocker des volumes de données, sur le long terme, ou encore pouvoir les partager entre serveurs et donc, entre applications.

## 2. Gestion des ressources

Que ce soit dans les infrastructures de type "cloud" ou dans les infrastructures plus traditionnelles, on retrouve toujours l'**utilisation de la virtualisation afin de segmenter les ressources en plusieurs systèmes autonomes (des VMs)**.

On peut voir la VM comme une portion de ressources, réservée pour un système, et qui sera isolée des autres VMs présentes sur un même serveur.

## 3. Exploitation des ressources

Afin d'exploiter les ressources mis à disposition par la virtu, sous forme de VM, on va y installer des OS.

**L'OS est le logiciel qui a la tâche de permettre l'exécution de plusieurs applications sur une machine donnée.**  
C'est aussi lui qui fournira les premiers remparts de sécurité autour des applications, afin qu'elles s'exécutent dans de bonnes conditions, sans perturber le fonctionnement d'autres applications.
Enfin, il est en mesure de fournir une myriade d'autres logiciels, pour assurer le bon fonctionnement des applications :

- gestion de l'heure
- gestion de logs
- gestion de service
- gestion du réseau
- gestion de mise à jour
- etc.

## 4. Applications

L'OS est en place, on peut alors y déployer des applications. L'application elle-même, ainsi que ses dépendances, et les données sur lesquelles elle repose.

On appelle "environnement d'exécution" tout ce qui est nécessaire pour qu'une application s'exécute : un OS qui gère l'application et ses processus, des fichiers de conf, des données, le fait d'écouter sur un port, etc.

# II. Une Infra Cloud

![There is no cloud](./pics/there_is_no_cloud.png)

➜ **Le terme "Infrastructure Cloud" désigne simplement des techniques modernes pour administrer et gérer une infrastructure**.

"Cloud" parce que c'est né avec les gros acteurs du "Cloud", ces fournisseurs de services en ligne (dont nous discutons plus bas).

➜ **Les points-clés des infras de type cloud**, que l'on va redéfinir tout au long de cette partie :

- automatisation
- abstraction
- standardisation

> On va revisiter les points abordés dans la première partie, un par un, afin de présenter en quoi les infrastructures de type Cloud différent des infrastructures plus traditionnelles, et en citant des exemples de technos qui permettent d'accéder à ces techniques plus modernes.

![Schéma infra cloud](./pics/cloud_infra.png)

> Des noms de technos sont cités sur le schéma. J'ai trouvé ça plus parlant que de mettre une description des technos. C'est pas exhaustif dukoo.

## 1. Les ressources

Le matériel physique n'est pas différent pour une Infra Cloud par rapport à une infrastructure traditionnelle.

On observe quand même des tendances, qui évoluent de concert avec les infras cloud :

- le stockage local est délaissé, au profit des baies de stockage, accessibles à travers le réseau
  - c'est à dire qu'on ne branche plus plein de disques sur les serveurs directement
  - mais on permet aux serveurs d'accéder à du stockage à travers le réseau
- le stockage étant déporté, on va de plus en plus vers des grosses baies de compute, avec uniquement des serveurs et peu de disques locaux
- les gros acteurs du cloud comme Amazon ou [OVH](https://www.ovhcloud.com/fr/) mettent au  des techniques de plus en plus élaborées pour la gestion de parcs à la taille démesurée

## 2. Gestion des ressources

➜ Afin de gérer plusieurs hyperviseurs, plusieurs machines physiques, ont vu le jour des solutions de "*cloud computing*".  
Les aspects principaux de ces solutions sont les suivants :

- **on regroupe les hyperviseurs pour en faire des "pools" de ressources**
  - un serveur avec un hyperviseur installé n'est vu que comme du CPU et de la RAM disponible
  - une flotte d'hyperviseurs, c'est donc juste un gros pool de CPU et de RAM
- on définit des endroits où il est possible d'obtenir du **stockage**
  - local aux hyperviseurs (disques branchés directement sur le serveur)
  - à travers le réseau
- on crée des **réseaux virtuels**
  - on ne s'occupe pas de savoir si telle VM et telle VM sont bien connectées entre elles
  - on se contente de les attribuer au même réseau virtuel, et ça ping
  - même si physiquement, elles sont sur des hyperviseurs différents
- **il est ensuite possible, sur une interface utilisateur simple, de créer des machines virtuelles**
  - on pourra alors profiter de fonctionnalités de HA natives par exemple
- fournir **un accès programmatique à la création de VMs**
  - y'a une API quoi !
  - il sera possible, depuis du code, d'interagir avec des VMs
  - cela permet notamment de pouvoir automatiser la création de VMs

➜ **L'idée est d'abstraire les ressources physiques.**  
A la création d'une machine virtuelle, on ne se pose pas la question de savoir où elle va tourner, ou si tel ou tel serveur sera prêt à l'accueillir : on a accès à un gros pool de RAM et de CPU, et on y lance notre VM.

> Malgré tout, l'aspect physique présente toujours des contraintes, principalement des contraintes de latence. Pour des fonctionnalités de réplicaiton de données entre deux de bases de données par exemple, il sera souvent conseillé que ces bases soient géographiquement proches, pour assurer la plus haute fiabilité possible concernant la réplication.

### A. Cloud Computing

Quelques solutions de *cloud computing* :

- [Openstack](https://www.openstack.org/)
- [Open Nebula](https://opennebula.io/)

L'interface d'[AWS](https://aws.amazon.com/fr/) ou de [GCP](https://console.cloud.google.com) fournissent des fonctionnalités similaires à OpenStack ou Open Nebula.

### B. Virtualisation

Les hyperviseurs que l'on voit dans les infras de type Cloud sont principalement [ESXi](https://www.vmware.com/fr/products/esxi-and-esx.html)/vSphere de VMWare, ainsi que le projet libre et open-source [KVM](https://www.redhat.com/fr/topics/virtualization/what-is-KVM).

> Il en existe bien sûr d'autres, que l'on voit moins souvent.

### C. Automatiser la création de VMs

Pour l'automatisation de création de machines virtuelles, cela va dépendre de la plateforme utilisée pour créer les VMs, ainsi que de la plateforme de *cloud computing* qui est choisie.

Citons cependant le projet [Terraform](https://www.terraform.io/) qui se veut agnostique au plus possible et qui est compatible avec toutes les plateformes de *cloud computing* modernes.

> Nous utiliserons en cours [Vagrant](https://www.vagrantup.com/) qui permet de tester ces fonctionnalités de création automatisée de VMs localement.

## 3. Exploitation des ressources

L'exploitation des ressources se fait toujours *via* l'installation d'un OS.

On trouve **beaucoup** d'OS GNU/Linux dans les environnements Cloud, bien qu'il ne soit pas exclusif.

Cette philosophie d'automatisation s'étend à la gestion des OS. Il va nous falloir être en mesure de :

- installer rapidement des nouvelles machines
- configurer l'OS de façon automatisée
- maintenir la machine dans le temps, de façon automatisée

Pour répondre à ces besoins, on va s'armer de deux outils principaux :

- **des OS adaptés**
  - la plupart des gros éditeurs ont sorti des versions adaptées au Cloud de leurs OS
  - ce sont des OS qui ont une configuration par défaut très minimale (et donc légère)
  - on peut les configurer de façon automatisée dès le premier boot, sans outil additionnel : l'OS s'auto-configure au premier boot
  - nous utiliserons principalement du Ubuntu et des bases Red Hat pour les cours, ainsi que l'utilisation de [cloud-init](https://cloud-init.io/)
- **des outils de gestion et déploiement de configuration**
  - on parle ici d'outils capables de déployer de la configuration sur plusieurs machines en même temps
  - mais aussi d'offrir un endroit centralisé où toute la configuration de toutes les machines est stockée
  - nous utiliserons principalement [**Ansible**](https://docs.ansible.com/ansible/latest/index.html) en cours

> Nous aurons un cours dédié sur les outils de gestion et déploiement de configuration.

![Ansible](./pics/ansible.png)

## 4. Applications

Tout un écosystème a vu le jour pour le développement, la livraison, le déploiement, et le maintien des applications qui vivent au sein des OS.

Les conteneurs jouent un rôle-clé dans ces infrastructures Cloud en permettant d'avancer vers l'abstraction et l'automatisation.

### A. La conteneurisation

➜ **Un conteneur, c'est une *charge applicative*.** C'est une boîte, un environnement, isolé et autonome, qui exécute une application.

Le conteneur permet, étant isolé et autonome, de s'abstraire du système où il s'exécute.  
Peu importe l'OS, peu importe la localisation géographique ou la temporalité, le fait de lancer un conteneur est indépendant de ce qui l'entoure.

Ainsi, peu importe l'application conteneurisée, pour l'OS, tous les conteneurs sont vus de la même façon : le conteneur permet donc une standardisation du lancement des applications.

➜ Aussi, le conteneur est une ressource qu'il est facile de créer, de maintenir, ou de supprimer, et ce, de façon rapide et automatisée.

On utilisera principalement en cours [Docker](https://www.docker.com/) pour la conteneurisation, et nous aborderons les fonctionnalités de [Kubernetes](https://kubernetes.io/fr/).

> Nous aurons un cours dédié sur la conteneurisation.

### B. La CI/CD

> A venir. Pour le moment non abordé en cours.

# III. Bénéfices

OK MAIS POURQUOI ? Quels sont les bénéfices de ces nouvelles techniques, ces nouveaux procédés, et ces nouvelles technos ?

> On ne parlera que de quelques bénéfices, tant il serait possible de disserter longtemps sur le sujet.

Rappelons les points-clés autour desquels se développent les infras cloud :

- automatisation
- abstraction
- standardisation

## 1. Securité

L'automatisation permet de rendre les procédés plus rapides certes, mais surtout **répétables et donc plus fiables**.

> En effet, après avoir déployé de façon automatisée 100x le même bail, et en ayant ajusté le tit petit à petit, à chaque déploiement, on peut être confiant quant à la robustesse du déploiement, sa fiabilité.

La standardisation amenée par le déploiement automatisé de configuration, ou l'utilisation de conteneurs amène aussi à **une meilleure gestion du changement**.  
Il est plus aisé, grâce à l'automatisation et la standardisation de ces pratiques, de maintenir dans le temps un système (que l'on parle d'une machine, d'un cluster de machines, ou d'un parc entier). Plus aisé de réagir aux évènements.

> Il n'a jamais été aussi simple de mettre à jour un paquet donné sur tout un parc, à un instant T, qu'avec des outils de déploiement comme Ansible.

L'automatisation et la standardisation tendent aussi à nous apporter la **conformité**, et donc une maîtrise du parc informatique.  

> En minimisant les actions manuelles, et en consignant tout déploiement dans les fichiers d'un outil comme Ansible, on garde une trace exhaustive de tout ce qui est déployable et de tout ce qui a été déployé.

## 2. Optimisation et performances

L'abstraction apportée par les solutions de *Cloud Computing* vis-à-vis du matériel permet une meilleure segmentation des ressources. Il est plus aisé de définir des environnements sur-mesure pour telle ou telle application, et ce façon très flexible, en profitant nativement de fonctionnalités de HA avancées.

De plus, il devient récurrent pour les entreprises, à l'ère du Cloud, d'externaliser une partie de leurs ressources chez des acteurs qui louent des serveurs accessibles en ligne (comme [Vultr](https://www.vultr.com/), OVH, ou encore AWS, GCP, Azure, etc).
Ces acteurs qui fournissent des ressources en ligne gèrent ainsi des parcs informatiques aux tailles démesurées. On pourrait alors parler de "grossiste" de la fourniture de ressources, qui ont tout intérêt à optimiser au plus la gestion de leur parc pour garder la complexité sous contrôle.

## 3. Réduction des coûts

L'automatisation permet une réduction des coûts, si l'on considère la rapidité pour déployer de nouvelles solutions, ou encore maintenir ce qui est déjà en place.

> Les processus automatisés et répétables permettent de ne pas réinventer la roue à chaque fois qu'on a besoin de rouler.

La standardisation des pratiques permet elle aussi de tendre vers une réduction des coûts : les bonnes pratiques, aussi bien pour la sécurité que pour les performances sont connues

Pour les clients de plateformes Cloud comme OVH ou AWS, on notera aussi la possibilité de consommer les ressources à la demande, ce qui amène une réduction des coûts immédiate, suivant les cas d'utilisation.

> On peut très bien louer une machine pour seulement quelques minutes, voire secondes, et ne payer que ce qui été consommé.

![More cloud ?](./pics/more_cloud.png)

# IV. Vocabulaire lié

## 1. Infrastructure as Code

L'infrastructure-as-code désigne le fait de stocker dans des fichiers texte une description de l'infrastructure.

**De façon très concrète, on parle des outils qui permettent de déployer des machins, à partir de fichiers texte** :

- création de VM
  - on peut demander le déploiement des machines virtuelles avec des fichiers texte
  - comme Vagrant en cours
- gestion et déploiement de conf
  - on peut demander le déploiement de configuration avec des fichiers texte
  - comme Ansible en cours
- conteneurisation
  - on peut demander l'exécution d'applications avec des fichiers texte
  - comme Docker en cours
  - c'est le cas de Kube aussi

Ces outils sont omniprésents dans les environnements Cloud.

## 2. Devops

> Beaucoup de bullshiet autour de cette notion, et on a tous notre petite définition perso.

En bref et concis, **la philosophie DevOps consiste à apporter des pratiques qui étaient habituellement réservées aux développeurs dans les habitudes d'administration et gestion d'infra.**

➜ Ca permet donc à des dévs de faire de l'administration avec une approche de dév.

➜ Et ça permet aux admins de profiter des bénéfices liés à l'utilisation de ces pratiques de dév.

Concrètement :

- on fait de l'Infra-as-Code
  - en utilisant des outils comme Terraform, Ansible, Docker, etc.
- on stocke toussa dans des dépôts Git
- on utilise des outils adaptés pour effectuer des tests sur nos fichiers

> Ici on utilise le mot "test" comme il est utilisé en développement. Effectuer des tests c'est vérifier la conformité et le bon fonctionnement des fichiers.

![2001 sysadmins be like](./pics/2001_sysadmins.jpg)

## 3. Fournisseur Cloud

Fournisseur Cloud ou *Cloud Provider* désigne les entreprises qui louent des ressources informatiques en ligne.

La location de serveurs physiques, de machines virtuelles, ou d'hébergements web rentrent tous dans cette case.

On peut citer par exemple OVH, Amazon AWS, [Scaleway](https://www.scaleway.com/en/), Vultr, [Digital Ocean](https://www.digitalocean.com/), Google GCP, etc.

## 4. as a Service

On désigne ici les termes IaaS, PaaS et SaaS. Ces trois termes désignent simplement les catégories d'offres qui existent chez les fournisseurs de services en ligne, comme les *cloud provider*.

### A. SaaS

Le client consomme juste une application en ligne. Il n'a rien besoin de connaître en informatique technique, il paie juste un service en ligne.

Quelques exemples de SaaS : Spotify, Discord, Youtube, etc.

Le fournisseur cloud gère toute la partie technique, le client n'est que consommateur.

### B. PaaS

Le client souscrit à une offre de type PaaS s'il veu gérer lui-même le déploiement de ses applications.

Il n'a pas besoin de gérer des problématiques de réseau, de stockage, ou de ressources de façon générale : il se contente de déployer des applications et payer les ressources qui ont été effectivement consommée.

Le fournisseur cloud gère les ressources physiques, gère le réseau, le stockage, la virtualisation et fournit au client un moyen de déployer ses applications, le plus simple possible.

Exemples de PaaS : la location de cluster Kubernetes en ligne, ou de plateformes comme [Heroku](https://www.heroku.com/).

### C. IaaS

Les offres IaaS sont celles qui donnent le plus de maîtrise au client. Le client souscrit à ce genre d'offres quand il souhaite gérer lui-même l'installation des OS et leur maintien, et parfois aussi avoir la main sur des contraintes physiques.

> Par "avoir la main sur des contraintes physiques" on entend que le client gérera lui-même des problématiques comme -par exemple- le fait d'avoir besoin de multiples serveurs sur différents continents dans le cas d'une application utilisée mondialement, dans le but de réduire la latence.

Le fournisseur Cloud qui est derrière ce type d'offre a toujours pour charge de gérer la partie matérielle, physique. Il gère donc aussi le réseau et le stockage.

Exemples de IaaS : offres comme celles proposées par OVH ou AWS par exemple, avec la location de serveurs dédiés, de VPS, ou même de baies entières.

## 5. Cloud-native

Le terme *Cloud-native* est de plus en plus utilisé pour désigner les nouveaux outils d'administration, les nouvelles applications, qui ont été conçues avec le monde du Cloud en tête.

On parle par exemple d'outils qui :

- interagissent naturellement avec les conteneurs
  - exemple : le monitoring c'est bien, le monitoring qui remontent des données par conteneur, c'est mieux, et celui par pod Kubernetes encore plus spécifique et pertinent
- Infrastructure-as-code
  - si tout est stockable dans des fichiers, automatisable, et répétable, on s'en approche
- interagissent naturellement avec les *cloud providers*
- sont déployables de façon automatisée, nativement

# V. Acteurs du cloud

## 1. Fournisseurs Cloud

Les fournisseurs Cloud, de OVH à Heroku en passant par Digital Ocean ont un rôle central dans tout cet écosystème : ce sont les grossistes. C'est eux qui possèdent d'énormes centres de données optimisés au sein desquels ils proposent la location de ressources.

![Stop saving your stuff here](./pics/stop_saving_stuff_here.png)

## 2. Open Source et Libre

Les concepts d'Open Source et de Libre sont eux aussi centraux dans cet écosystème. Beaucoup des solutions techniques liés au Cloud sont des solutions libres et Open Source.

Des acteurs comme RedHat, Netflix ou Google son réputés pour être transparents sur les techniques qu'ils utilisent, et pour être de gros contributeurs de l'Open Source.

## 3. CNCF

La [CNCF](https://www.cncf.io/) est une initiative rattachée à la Linux Foundation qui vise à promouvoir certains outils, certaines technos, particulièrement adapté(e)s aux environnements Cloud.

L'initiative est aussi soutenue par de grands acteurs de l'IT comme Intel, Microsoft, RedHat, Google, etc.

# VI. P'tit bilan fast

Le *Cloud* désigne tout ce qui touche de près ou de loin à la fourniture de services en ligne.

On parle *d'Infrastructure Cloud* quand on utilise des plateformes de *cloud computing* pour gérer la virtualisation ; que ce soit installé en interne, avec des outils comme OpenStack ou OpenNebula, ou fourni en ligne avec par exemple AWS ou OVH.

Pour prolonger les valeurs du *Cloud*, des outils *d'Infrastructure-as-Code* sont utilisés pour :

- créer des machines virtuelles (Vagrant, Terraform)
- configurer les OS (Linux + cloud-init + Ansible)
- déployer les applications (Docker + Kubernetes)

C'est tout cette chaîne que nous allons approfondir pendant le cours, d'un point de vue technique.

![Infra cloud](./pics/cloud_infra.png)