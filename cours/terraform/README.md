# Terraform

Terraform est un outil utilisé pour automatiser la création de machines virtuelles ; il est analogue à Vagrant, que nous utilisons depuis le début du cours.

La différence entre Terrafom et Vagrant réside principalement dans les solutions avec lesquels ils sont compatibles :

- Vagrant
  - destiné à un usage particulier
  - création de VMs locales sur un poste
  - permet de piloter des solutions d'hyperviseurs locaux
    - VirtualBox
    - Workstation
    - etc.
- Terraform
  - déstiné à un usage particulier ou pro
  - création de
    - VMs locales ou distantes
    - autres ressources
  - permet de pilotes des hyperviseurs ou Cloud Provider
    - hyperviseurs
      - ESXi/vSphere
      - Xen
      - etc.
    - Cloud Providers
      - OpenNebula
      - OpenStack
      - AWS
      - GCP
      - Azure
      - etc.

Vagrant est donc plutôt destiné à du lab local, pour déployer quelques VMs.  
Terraform est plutôt destiné à des déploiements complexes, orientés production, et permet de déployer un parc entier.

De la même façon qu'il était nécessaire de maîtriser, par exemple, VirtualBox avant de pouvoir utiliser Vagrant, il va être necessaire de comprendre la plateforme destination avant de pouvoir automatiser son utilisation à l'aide de Terraform.