# Cloud B3 2021

Ici seront hébergés les supports de cours du cours de Cloud B3 2021.

Le but de ce cours est d'explorer les techniques modernes de gestion d'infrastructure.

### Cours

- [Intro Cloud](./cours/cloud/README.md)
- [Ansible](./cours/ansible/README.md)

### TPs

- [TP0 : Vagrant & Ansible chill](./tp/0/README.md)
- [TP1 : cloud-init](./1/README.md)
- [TP2 : Ansible](./tp/2/README.md)
- [TP3 : Conteneurs](./tp/3/README.md)
- [TP4 : Terraform](./tp/4/README.md)

![Un potit schéma](./cours/cloud/pics/cloud_infra.png)
