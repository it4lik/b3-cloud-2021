# TP0 : Vagrant + Ansible chill

Le but de ce TP est de vous faire appréhender l'automatisation dans des environnements GNU/Linux.

Au menu :

- utilisation de Vagrant pour déployer des VMs
- utilisation d'Ansible pour déployer des applications et de la configuration sur les VMs

<!-- vim-markdown-toc GitLab -->

- [TP0 : Automatisation](.tp0--automatisation)
- [I. Vagrant](#i-vagrant)
  - [1. Mise en jambe](#1-mise-en-jambe)
  - [2. Accélération du déploiement](#2-accélération-du-déploiement)
  - [3. Repackage](#3-repackage)
  - [4. Ajout d'un node](#4-ajout-dun-node)
- [II. Ansible](#ii-ansible)
  - [1. Un premier playbook](#1-un-premier-playbook)
  - [2. Création de playbooks](#2-création-de-playbooks)
    - [A. Déploiement base de données : MariaDB](#a-déploiement-base-de-données--mariadb)
    - [B. Déploiement comptes utilisateurs](#b-déploiement-comptes-utilisateurs)
    - [C. Configuration du firewall](#c-configuration-du-firewall)

<!-- vim-markdown-toc -->

# I. Vagrant

> Vagrant doit être installé sur VOTRE PC. Il sert de surcouche à votre hyperviseur (VirtualBox, Hyper-V, autres). Toutes les commandes `vagrant` sont donc à effectuer sur votre PC.

Ici, on va mettre en place un environnement virtualisé avec Vagrant. Les exemples seront donnés avec une box CentOS/7.

> Ui c'est outdated, c'est une box simple d'utilisation qu'on retrouve utilisée partout, c'est cool pou run cas d'école, pour un exemple, on va préférer des choses à jour pour le reste du cours.

## 1. Mise en jambe

> Ces étapes sont à réaliser sur votre machine, PAS dans une VM.

```bash
# Créez vous un répertoire de travail SUR VOTRE MACHINE TOUJOURS :)
$ mkdir vagrant
$ cd vagrant

# Initialisez un Vagrantfile
$ vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

La commande `vagrant init` permet de générer un fichier `Vagrantfile`. Ce fichier contient tout le nécessaire pour allumer une VM CentOS7.

> Je vous invite à lire le `Vagrantfile`, vous y trouverez beaucoup de configurations par défaut qui peuvent être intéressantes.

Une fois le `Vagrantfile` généré, épurez-le en enlevant les commentaires, faites en sorte de vous retrouver avec ça :

```ruby
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé
  # Ca ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.
  # Pas de temps à perdre avec ça :)
  config.vm.synced_folder ".", "/vagrant", disabled: true

end
```

Pour lancer la VM :

```bash
$ vagrant up
[...]

# Vous pouvez voir l'état des VMs liées au Vagrantfile
$ vagrant status

# Une fois le déploiement terminé vous pouvez SSH dans la VM grâce à Vagrant
$ vagrant ssh
```

## 2. Accélération du déploiement

**Quand on lance une VM avec Vagrant, on peut lui demander d'exécuter des commandes avant d'être considérée comme "prête".** Utile pour préparer un peu l'environnement de la VM, avant de commencer à travailler dedans.

A côté de ça, **il existe un autre mécanisme de Vagrant : le packaging.** Le packaging permet de transformer une VM qui a été lancée avec Vagrant en une nouvelle box.

On va donc pouvoir :

- lancer une VM avec Vagrant à partir de la box `centos/7`
- demander l'exécution de commandes à l'intérieur de la VM
  - mise à jour du système
  - installation de paquets
  - configuration du système
- une fois allumée, on peut convertir la VM qui tourne en une nouvelle box : `centos/ynov`
  - avec une commande `vagrant package`
- cette box `centos/ynov` sera toute prête à l'emploi
  - elle contient nos modifications (mise à jour du système, paquets, configurations, etc.)
  - à chaque fois qu'on créera une VM à partir de cette box, elle contiendra déjà ces modifications
  - un peu comme si on avait créé notre propre `.iso` !

---

Définissez le `Vagrantfile` suivant (un nouveau dans un nouveau répertoire de travail, ou remplacez l'ancien, peu importe) :

```ruby
Vagrant.configure("2") do |config|
  config.vm.box = "centos/7"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true
  
  # Exécution d'un script au démarrage de la VM
  config.vm.provision "shell", path: "setup.sh"
end
```

Et, dans le même dossier que le `Vagrantfile`, créez le fichier `setup.sh` contenant :

```bash
#!/bin/bash

# Ajout des dépôts EPEL
yum install -y epel-release

# Mise à jour du système
yum update -y

# Installation d'un paquet
yum install -y vim

# On active la connexion au serveur SSH avec un mot de passe
# Ui c'est nul, mais on en a besoin ensuite
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
```

Comme défini dans le `Vagrantfile`, le script `setup.sh` sera exécuté au lancement de la VM.

Une fois ces deux fichiers en place, vous allez pouvoir lancer la VM.  
Avec le script qui update étou, ça va être un peu long. Justement le but c'est de pas se le taper à chaque fois, c'est pour ça qu'on repackage une box.

```bash
# On allume la VM
$ vagrant up

# Vous devriez voir l'exécution du script se dérouler dans la sortie.
# C'est long :((((

# On vérifie l'état de la VM
$ vagrant status

# Vous pouvez aussi ouvrir votre hyperviseur et constater qu'il y a une nouvelle VM.
```

**Lorsque la VM sera allumée (avec les installations réalisées), on va pouvoir repackager la box :**

```bash
# On convertit la VM en un fichier .box sur le disque
# Le fichier est créé dans le répertoire courant si on ne précise pas un chemin explicitement
$ vagrant package --output centos-ynov.box

# On ajoute le fichier .box à la liste des box que gère Vagrant
$ vagrant box add centos-ynov centos-ynov.box

# On devrait voir la nouvelle box dans la liste locale des box de Vagrant
$ vagrant box list
```

Enfin, il ne reste qu'à tester, en créant un nouveau `Vagrantfile` :

```ruby
Vagrant.configure("2")do|config|
  # Notre box custom
  config.vm.box="centos-ynov"
end
```

🌞 Go `vagrant up` !

- puis `vagrant status` pour vérifier l'état de la VM
- vérifiez que vous pouvez `vagrant ssh` dans la VM
- vérifiez que la VM contient vos modifications

> Par défaut, Vagrant synchronise tout le répertoire contenant le Vagrantfile dans la VM au démarrage de celle-ci. Créez donc un répertoire dédié afin de ne pas synchroniser de choses superflues.

## 3. Repackage

🌞 Modifier le Vagrantfile (et/ou le `setup.sh`) pour de nouveau **repackager une box par vous-mêmes** :

- repartez de la box `centos-ynov` ! Elle est déjà à jour
- ajout de paquets
  - le paquet `vim`
  - le paquet `ansible`

🌞 **Test !**

- lancez une VM qui part de votre box repackagée et vérifiez la présence des commandes `vim` et `ansible`

## 4. Ajout d'un node

Il est parfaitement possible de lancer plusieurs VMs d'un coup à l'aide de Vagrant.

La syntaxe est la suivante :

```ruby
Vagrant.configure("2") do |config|
  
  # Configuration commune à toutes les machines
  config.vm.box = "centos-ynov"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false
  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 
  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
  end

  # Config une première VM "node1"
  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "xxx.xxx.xxx.xxx"
  end

  # Config une première VM "node2"
  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "xxx.xxx.xxx.xxx"
  end
end
```

Une fois qu'on a `vagrant up`, il est possible d'accéder aux différentes machines en précisant leur nom sur la commande pour s'y connecter : `vagrant ssh node1` par exemple.

> On peut connaître le nom des machines et leur état à tout moment avec un `vagrant status`.

🌞 Ecrivez un `Vagrantfile` qui déploie 3 machines dans le même réseau

- vérifiez que vous pouvez vous connecter aux 3 machines
- vérifiez qu'elles peuvent se `ping`
- vérifiez que vous pouvez vous connecter en SSH d'une machine à l'autre
  - utilisateur : `vagrant`
  - mot de passe : `vagrant`

# II. Ansible

Pour cette partie, on va réutiliser le `Vagrantfile` que vous avez rédigé précédemment.

Renommez simplement les 3 machines. **SVP faites-le** pour que ce soit clair :

- une machine `admin.tp0.ynov`
  - ce sera votre machine d'administration
  - vous exécuterez les commandes `ansible` depuis cette machine
  - c'est juste qu'il est conseillé de faire tourner Ansible sur une machine Linux, donc ça me permet de vous faire travailler facilement dans un environnement Linux
  - normalement, on utilise notre propre PC
- deux autres machines : `node1.tp0.ynov` et `node2.tp0.ynov`
  - ce seront les machines sur lesquelles on déploiera la config

Ici, vous allez vous faire la main sur vos premiers *playbooks* Ansible.

Un peu de vocabulaire lié à Ansible :

- ***inventory***
  - l'inventaire ou *inventory* est la liste des hôtes
  - les hôtes peuvent être rasemblés dans un groupe qui porte un nom
- ***task***
  - une tâche ou *task* est une opération de configuration
  - par exemple : ajout d'un fichier, création d'un utilisateur, démarrage d'un service, etc.
- ***role***
  - un rôle est un ensemble de tâches qui a un but précis
  - par exemple :
    - un role "apache" : il installe Apache, le configure, et le lance
    - un role "users" : il déploie des utilisateurs sur les machines
- ***playbook***
  - un *playbook* est le lien entre l'inventaire et les rôles
  - un *playbook* décrit à quel noeud on attribue quel rôle

## 1. Un premier playbook

Juste pour jouer, mettre en place Ansible et appréhender l'outil, on va rédiger un premier playbook dans un seul fichier.  
En temps normal, les fichiers Ansible sont éclatés dans plein de fichiers différents pour garder l'ensemble maintenable.

> Créez un répertoire dans votre répertoire personnel (à l'intérieur de `/home`) afin de stocker tous les fichiers Ansible. **Vous n'avez PAS BESOIN d'utiliser le dossier `/etc`**.

---

On va ajouter simplement un serveur Web à notre machine déployée par Vagrant.

🌞 **Mettez en place une connexion SSH sans mot de passe** (par échange de clés) de la machine `admin.tp0.ynov` (le "control node") vers les deux autres machines `node1.tp0.ynov` et `node2.tp0.ynov`

- Ansible passe à travers SSH donc il est nécessaire qu'on ait une connexion SSH fonctionnelle entre la machine qui a la conf et les machines qui la reçoivent

🌞 **Créez vous un répertoire de travail**

- dans votre homedir
- tous les fichiers liés à Ansible devront se trouver dedans

🌞 **Créez un playbook minimaliste `nginx.yml` :**

- le nom `ynov` dans `hosts: ynov` fait référence au fichier `hosts.ini` que vous allez créer juste après

```yaml
---
- name: Install nginx
  hosts: ynov
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started
```

🌞 **Et créez un inventaire `hosts.ini` :**

- pour cette étape, vous devrez spécifier les IP des VMs `node1.tp0.ynov` et `node2.tp0.ynov`

```ini
[ynov]
<node1_IP>
<node2_IP>
```

🌞 **Enfin, créez un fichier `index.html.j2` :**

- la balise dans ce fichier contient une variable Ansible
- la variable sera dynamiquement remplacée par la vraie IP adresse du serveur sur lequel on déploie le fichier, au moment où on déploie la conf

```bash
Hello from {{ ansible_default_ipv4.address }}
```

🌞 **Lancez le playbook !**

- cette commande va se connecter en SSH aux machines de destination déclarée dans le fichier `hosts.ini` et déployer la configuration demandée dans `nginx.yml`

```bash
$ ansible-playbook -i hosts.ini nginx.yml
```

**Vérifier le bon fonctionnement du site web !**

## 2. Création de playbooks

Il y a trois playbooks à réaliser dans cette partie : `maria.yml`, `users.yml` et `firewall.yml`.

### A. Déploiement base de données : MariaDB

🌞 **Créez un playbook `apache.yml` qui :**

- installe MariaDB
- lance MariaDB
- ajoute une base de données

### B. Déploiement comptes utilisateurs

🌞 **Puis un playbook `users.yml` qui :**

- ajoute un utilisateur `admin`
- lui déposer votre clé SSH publique afin de pouvoir s'y connecter
  - [utilisez le module `authorized_key`](https://docs.ansible.com/ansible/latest/modules/authorized_key_module.html)

### C. Configuration du firewall

🌞 **Enfin un playbook `firewall.yml` qui :**

- ouvre le port 80 et le port 22
- s'assure que le port 8000 est fermé
