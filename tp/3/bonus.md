# TP3 : Bonus

Ce bonus du TP3 se consacre aux aspects suivants :

- mise en évidence de la sécurité autour des conteneurs
- mise en évidence du fonctionnement bas-niveau des conteneurs :
  - ce sont de simples processus
  - isolés grâce aux *namespaces*
  - restreints grâces aux *cgroups* et aux *capabilities*
- utilisation de Podman
  - outil plus sécurisé par défaut

# Sommaire

- [TP3 : Bonus](#tp3--bonus)
- [Sommaire](#sommaire)
- [I. Docker](#i-docker)
  - [1. Isolation you said ?](#1-isolation-you-said-)
  - [2. Capabilities](#2-capabilities)
- [II. Podman](#ii-podman)

![I'm afraid to ask](./pics/containers_afraid_to_ask.jpg)

# I. Docker

## 1. Isolation you said ?

**A ce stade, nous avons vu qu'un conteneur n'est qu'un simple processus** : une fois un conteneur lancé, le processus qu'il exécute est visible dans l'arborescence de processus de l'hôte avec un classique `ps -ef`.

> Accessoirement, vous avez sûrement remarqué que par défaut, TOUT tourne avec l'identité de l'utilisateur `root`.

Le démon Docker a pour but de préparer le terrain afin de créer un environnement dans lequel va s'exécuter le conteneur. Il utilise le mécanisme de *namespaces* pour isoler les processus qu'il lance et en faire des "conteneurs".

Dans cette partie, on va se concentrer à explorer l'isolation mise en place par les *namespaces*.

➜ **Visualiser l'isolation**

```bash
# assurez-vous qu'aucun conteneur ne tourne
$ docker ps
$ docker rm -f $(docker ps -aq)

# lancez un conteneur de test, qui sera facilement identifiable
$ docker run -d fedora sleep 999999

# visualiser l'isolation : on récupère un shell dans le conteneur
$ docker exec -it $(docker ps -lq) bash

# on a un shell dans le conteneur, explorons quelques aspects :
# réseau
$ ip a
# user
$ cat /etc/passwd
# process
$ ps -ef
# hostname
$ hostname
# d'autres aspects sont isolés, mais moins facilement visibles : la gestion de l'heure, ou les IPC
```

➜ **Les namespaces**

Les ***namespaces*** du noyau Linux sont les responsables de cette isolation. Quand un namespace est créé, un identifiant unique lui est attribué.

Il est possible à tout moment de consulter dans quels *namespaces* s'exécute un processus donné, en utilisant l'API `/proc/`. Let's go.

```bash
# consultons les identifiants des namespaces dans lesquels on évolue actuellement, avec le shell qu'on utilise actuellement
$ echo $$ # petit trick qui affiche le PID du shell actuel

# il existe dans /proc/ un dossier par processus, consultons le dossier concernant le shell actuel
$ ls /proc/$$/

# il y a ici un dossier ns, qui contient une référence vers les namespaces utilisés par notre shell actuel
$ ls -al /proc/$$/ns/
```

**Grâce à cette manip, on peut identifier les IDs uniques des *namespaces* dans lesquels évolue un processus donné.**

Si deux processus évoluent dans un *namespace* `network` différent, par exemple, alors ils auront chacun une stack réseau différente (= ils n'auront pas les mêmes cartes réseau visibles avec un `ip a`).

Il en va de même avec le *namespace* `pid` par exemple, qui donnera à un processus donné une visibilité différente de l'arbre des processus (que l'on consulte usuellement avec la commande `ps -ef`).

> Si on utilise pas d'outils comme de la conteneurisation, tous les processus d'un système Linux évoluent par défaut dans les même namespaces.

➜ **Comprendre l'isolation**

```bash
# assurez-vous qu'aucun conteneur ne tourne
$ docker ps
$ docker rm -f $(docker ps -aq)

# lancez un conteneur de test, qui sera facilement identifiable
$ docker run -d fedora sleep 999999

# repérez les namespaces de votre shell actuel
$ ls -al /proc/$$/ns

# repérez le PID du processus sleep 999999 exécuté par le conteneur
$ ps -ef | grep 99999
# repérez les namespaces dans lesquels il évolue
$ ls -al /proc/<PID>/ns
```

**Vous devriez constater que vos namespaces et ceux du conteneurs sont différents.** Enfin, presque tous. hihi.

## 2. Capabilities

**Les *capabilities* déterminent des droits que peut avoir un processus pendant son exécution.**  
Des sortes de superpouvoirs qu'on peut lui ajouter. Ce sont habituellement des droits qui sont réservés à l'utilisateur `root` mais certains processus ont légitimement besoin de droits plus élevés pour s'exécuter.

> Comme la commande `ping` qui a besoin d'une capabilities spécifique afin d'envoyer des paquets ICMP.

➜ **Visualiser les capabilities**

```bash
# la commande getpcaps est là pour ça
# on repère le PID d'un processus
$ ps -ef

# visualisation des capabilities
$ getpcaps <PID>

# vous pouvez consulter une liste des capabilities et leur fonction à tout moment
$ man capabilities
```

➜ **Docker & capabilities**

Puisque donner toutes les *capabilities* à un processus équivaut à l'exécuter en tant que `root`, il est nécessaire de restreindre les *capabilities* des processus exécutés par les conteneurs.

```bash
# on lance deux conteneurs, avec un jeu de capa différente
$ docker run -d fedora sleep 99999
$ docker run -d --cap-drop cap_net_bind_service fedora sleep 99999

# on repère les PIDs des deux processus
$ ps -ef | grep 99999

# on repère les capabilities à disposition des processus
$ getpcaps <PID>
$ getpcaps <PID>

# la capa cap_net_bind_service devrait être absente du deuxième
```

> Bon, de toute façon, tout tourne avec `root` par défaut, alors comme qui dirait : xD saserarien

![Docker Security](./pics/docker_secu.jpg)

# II. Podman

> Va falloir enlever Docker de la machine pour que Podman puisse s'installer correctement.

➜ **Installer Podman sur la machine**

- pour l'install, référez-vous à la [doc officielle](https://podman.io/getting-started/installation)

➜ **Le CLI `podman`**

C'est le même que `docker`. Avec des trucs en plus.

➜ **Le démon `podman`**

Y'en a pas. Hihi.

➜ **Explorer `podman`**

Ré-utilisez tout ce qu'on on a vu Dans la partie Docker pour explorer Podman :

- regardez les process lancés par Podman, en particulier les utilisateurs qui lancent ces process
- regardez les namespaces utilisés par Podman

Vous devriez remarquer que l'empreinte de Podman est beaucoup plus légère sur le système, et qu'il n'a jamais besoin de `root` pour fonctionner.

![So hot](./pics/container_secu_so_hot.jpg)

➜ **La notion de *pods***

Podman tire son nom du fait que les *pods* sont natifs. Ceux qui sont familiers avec Kubernetes connaissent déjà cette notion, elle est similaire dans Podman.

Un Pod est ensemble de conteneurs, qui fonctionnent ensemble afin de fournir un service.  
L'exemple classique : un serveur web et une base de données, chacun dans un conteneur distinct. On pourra alors les placer dans le même *pod*.

Cela permet ensuite de gérer cet ensemble de conteneurs comme une seule entité.

```bash
# création d'un pod
$ podman pod create --name meo

# ajout de conteneurs dans le pod
$ podman run -d --pod meo nginx
$ podman run -d --pod meo nginx

# interactions avec le pod
$ podman pod ls
$ podman pod logs meo
$ podman pod stop meo
$ podman pod start meo

# on peut générer une unité systemd ou un manifest kubernetes à partir d'un pod existant
$ podman generate systemd meo
$ podman generate kube meo
```
