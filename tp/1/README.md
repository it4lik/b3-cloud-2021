# TP1 : cloud-init

`cloud-init` est un outil qui permet d'initialiser la configuration du système, de l'OS, au moment du premier boot d'une machine.

Une fois que la machine est déployée et que l'OS y a été configuré avec `cloud-init`, la machine est prêt à accueillir de la configuration, des applications.

Ainsi `cloud-init` va notamment gérer :

- config réseau élémentaire (DNS, proxy, NTP, etc)
- configuration des utilisateurs et clés SSH
- update système, installation de paquets élémentaires
- configuration de systemd
- etc.

> Les tests dans ce TP seront réalisés avec une machine Ubuntu Focal 20.04 LTS. Vous pouvez décider de choisir un autre OS qui supporte `cloud-init`.

# Sommaire

- [TP1 : cloud-init](#tp1--cloud-init)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. Premiers pas](#i-premiers-pas)
- [II. Go further](#ii-go-further)
- [III. Ansible again](#iii-ansible-again)

# 0. Prérequis

- Vagrant + un hyperviseur installé (VirtualBox conseillé)
- avoir terminé le [TP 0](../0/README.md)
- avoir installé le plugin Vagrant pour la gestion de fichiers d'environnements
  - installez le avec la commande suivante : `vagrant plugin install vagrant-env`

**Si vous êtes sous Windows** il va être necessaire de réaliser quelques autres opérations pour que Vagrant puisse correctement utiliser `cloud-init`

- **vous devrez utilisez un shell Linux** (oui oui, sous Windows)
  - utilisez par exemple [*Git bash*](https://www.atlassian.com/fr/git/tutorials/git-bash) qui se télécharge vite, et fonctionne bien
  - vous utiliserez désormais *Git bash* à la place de Powershell (ou l'invite de commandes CMD)
- **il vous faudra le Windows ADK**
  - [téléchargeable ici, depuis la doc officielle](https://docs.microsoft.com/fr-fr/windows-hardware/get-started/adk-install)
  - choisissez bien la version pour votre OS
  - une fois l'installeur lancé, n'installez que les "Outils de déploiement" (ou "Deployment Tools") lorsque le choix vous sera proposé
- **il faudra ajouter un chemin à votre `PATH`**
  - le chemin dans lequel se trouve la commande `oscdimg.exe`
  - le chemin à ajouter est normalement `C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg\`
  - vérifiez dans votre terminal *Git bash* que vous avez accès à la commande `oscdimg` désormais

C'est tout bon, go next. **Les commandes `vagrant` devront désormais être effectuées depuis *Git bash*.**

# I. Premiers pas

> Créez un nouveau répertoire de travail afin de réaliser les opérations suivantes.

Créez un fichier `Vagrantfile` avec le contenu suivant :

```ruby
Vagrant.configure("2") do |config|
  config.env.enable

  config.vm.define "ubuntu-cloud-init"
  config.vm.box = "focal-server-cloudimg-amd64-vagrant"
  config.vm.box_url = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64-vagrant.box"

  config.vm.provider "virtualbox" do |v|
    v.name = "ubuntu-cloud-init"
  end

  config.vm.cloud_init do |cloud_init|
    cloud_init.content_type = "text/cloud-config"
    cloud_init.path = "data.yml"
  end
end
```

Juste à côté, créez un fichier `data.yml` :

```yml
#cloud-config

package_update: true
packages:
 - git

users:
 - name: it4
   sudo: ALL=(ALL) NOPASSWD:ALL
   groups: adm,sys
   home: /home/it4
   shell: /bin/bash
   ssh-authorized-keys:
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC35xnuCeDhQvh47jquJ4CTUg0NxG4tOsSzM+tGCSwIMJPBwz0Jw5PbdINGiDef+eFLD41B1dqcQi/OwYCDJJUlENxNKBj3EMlA2RL1UCWibEJaVoDUD2NrYiwwtKcPscuthImZwD2kAm+W7ITCaSbeyS5A4w/pVFyDAlLJsJtiLVWSvX+fKUWOYZgQgPhYgfYuTRebEU77Khd01mRFu76PhHiV/cfCecIO7O2R0431RIhtaDzWOuYwQKd/ORbWYbHmav9wqOvBs4IwBJYusRPJKoQ0+Gdq6XF9p1uB2Yf36Zh/xfyDxl7I8Qb2kQGseTP2J/Dd6OOBIN7b2Uq8uS5/okb5Qm9iqTKvWFVlSI83W9yQH25x4pkHqu3hxygfg45low/t3EvX6dRTodnnehAukFL8pQBsTeRoh4oxWIVxKWkpnGhRNdBiAfVvhYkCjsHlRiw8XyT/91MnnUSrzEmL4kixlBE++rBJyBnt9SK2oF9UDaSWrkqTzqLKzMOYrhJvbOGGYN7DQdo/Q8WQvYjWT2UYQJcYmOE0ow8Vomrw+EP66DGnO0ZjcNmGKz0VbQRiVWcUW+Or38mW8ye7nmuyF2wjP48pZ7JTDJNqitM3aVcbz3ZzqwcodiiU95pDVRhA7t69YqeIt6OchlqDz5qBLuyiR4zaWYHXfSGmeSIMgw== it4@nowhere.it4

runcmd:
  - git --version > /tmp/b3_git_version
```

Enfin, créez un dernier fichier `.env` :

```env
VAGRANT_EXPERIMENTAL="cloud_init,disks"
```

Une fois ces trois fichiers en place, allumez la machine avec `vagrant up`.

🌞 **Prouvez en vous connectant à la VM que les changements demandés dans le fichier cloud-init ont été effectués**

# II. Go further

Le but, avec `cloud-init` c'est de rendre la machine parfaite opérationnelle afin de venir y déployer des applications. Il serait donc intéressant d'installer de quoi déployer de la conf Ansible, ou éventuellement des conteneurs Docker.

🌞 **Modifiez le `Vagrantfile` (ou créez en un nouveau), faites en sorte que :**

- **Docker soit setup**
  - Docker installé (suivez [la doc officielle pour Ubuntu](https://docs.docker.com/engine/install/ubuntu/))
  - l'unité `docker.service` démarrée et active au boot de la machine (en cas de reboot)
  - création d'un utilisateur dédié
    - son nom est `docker-admin`, il est membre du groupe `docker`
    - le groupe `docker` est créé automatiquement à l'installation du paquet
      - ainsi, cet utilisateur doit pouvoir passer des commandes `docker` sans utiliser `sudo`
    - une clé SSH a été déployé pour pouvoir s'y connecter sans mot de passe
- **La machine doit être prête à recevoir de la conf Ansible**
  - `python` installé
  - création d'un utilisateur dédié
    - son nom est `ansible-admin`
    - un utilisateur qui a les droits `sudo` complets
    - une clé SSH a été déployé pour pouvoir s'y connecter sans mot de passe
- **La machine doit être configurée pour utiliser un serveur NTP spécifique**
  - il existe des serveurs NTP libres d'accès sur internet : https://www.pool.ntp.org/fr/
  - renseignez-vous pour savoir comment installer configurer un client NTP sur Ubuntu
  - faites vos tests à la main, avant de mettre tout ça dans `cloud-init`

> Dans le rendu, sur le dépôt git, je veux tous les fichiers que vous créez.

# III. Ansible again

Une fois la machine déployée avec `cloud-init`, elle est prête à recevoir de la conf. C'est un peu un ensemble de cellule-oeuf, et la machine n'a pas encore été spécialisée. Elle peut accueillir n'importe quelle application.

Après le passage de `cloud-init`, la machine est simplement conforme à ce qu'on attend de la configuration élémentaire d'un OS au sein de notre parc.

Allons-y, déployons de la conf ! Donnons une configuration spécifique à la machine.

🌞 **Déployer NGINX sur la machine avec Ansible**

- réutiliser les fichiers écrits au [TP 0](../0/README.md#1-un-premier-playbook)
- il sera peut-être nécessaire d'adapter un peu le contenu pour Ubuntu
- NGINX :
  - doit écouter sur le port 8080
  - doit servir une page HTML que vous aurez custom
    - contenu au choix
    - il sera stocké dans `/var/www/b3_tp1/index.html`
- vous devez utiliser les commandes `ansible`
  - depuis votre poste si vous êtes sous Linux (installez `ansible` sur votre poste)
  - depuis une VM dédiée (installez `ansible` dans la VM, comme au premier TP)
  - quoiqu'il en soit, il faudra une connexion SSH fonctionnelle vers la VM, utilisez `cloud-init` pour poser la clé adéquate

> Dans le rendu, sur le dépôt git, je veux tous les fichiers que vous créez.
